# Git repository lifecycle (simplified)
- Unstaged - just after a save 
- Staged - prepared all relevant files for saving ```git add .```
- Committed - saved the staged LOCALLY files ```git commit -m "SOME MESSAGE"```
- Pushed - push your committed files (savegame) to an online repository - to GitHub/GitLab/BitBucket ```git push```

- Cloned - take repository from online and download it
- Pulled